Visão Geral 

Planilha Excel com Macros que permite a integração com o sistema XTech de comércio eletrônico.

Funções

	Inclusão de novos produtos 
	Gerenciamento do estoque
	Gerenciamento de categorias

Como Usar

1. Configuração

	Na aba "Config" é necessário inserir a APP-KEY, a API-KEY (ambas disponíveis na área administrativa da loja) 
	e a url da loja (é necessário usar o domínio da xtech, ex: https://sualoja.xtechcommerce.com/)
	
	O arquivo acompanho um editor de interface para facilitar as operações. 

2. Adicionar Produtos

3. Gerenciar Estoques

4. Adicionar Categorias


Funções Utilizadas
	
	PostNewProducts() -> Adiciona novos produtos com base na aba New Products
	SendRequest() -> Envia a requisição para o site
	GetStock() -> Recebe os valores de estoque da loja
	UpdateStock() -> Envia para o site as informações de estoque, com base na planilha Products
	GetCategories() -> Atualiza a lista de categorias
	PostCategories() -> Insere novas categorias na loja

	